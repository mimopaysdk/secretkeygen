package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Domikado
	// stagingEncryptedText: FGruURTmWFRT2OXw1qyjBg==
	public static String msKeyValueStaging = "GCBJFHcQXShvZnVDTG57ME9kGhMyGWskdQJDTnxOe0c=";
	// gatewayEncryptedText: tdmZ/Mmk7tEf1lsOXosBhA==
	public static String msKeyValueGateway = "cUcmI017MHlZChFFcXFTIwQFFzYqXk5+X1syEGplSms=";
}
