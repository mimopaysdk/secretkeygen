package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: LeTang
	// stagingEncryptedText: APN0D1NAKfhtokUUr8kE9w==
	public static String msKeyValueStaging = "fy1yBH1pQlUPOANUeU5QbF9dPG14dkM9Kx5dHzZ4FB4=";
	// gatewayEncryptedText: 6ztb2GtEcIBZKypKTM9Rpg==
	public static String msKeyValueGateway = "VjgqXyhnfR1/OHsvZA5yUj4zJEgNB0Y6Ih5vRHhfRSo=";
}
