package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Rodcell
	// stagingEncryptedText: Ggsvp1ly+z+odQVM1rh8pA==
	public static String msKeyValueStaging = "ez4yF0U6cHhrGCx3GzgIXU14Zx9sHwcwNxFfMxkENH4=";
	// gatewayEncryptedText: Uzk5rqs3KuZaaETk+fobNw==
	public static String msKeyValueGateway = "WoBgd1ZMOXRCKxobHzBTYicuSxYGIicTfTYwSCZXVHc=";
}
