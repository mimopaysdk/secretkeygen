package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: KlikKayaInovasi
	// stagingEncryptedText: Ng38CEkCySfvmJ5Zbok2Kg==
	public static String msKeyValueStaging = "XyFoez18FlA0NHdYF2ghTF5tflFETDcZZwgwN2JReiM=";
	// gatewayEncryptedText: IwPh95HwNee2Mavnraubkg==
	public static String msKeyValueGateway = "aG5VFjULNUMeYQsbdjx7STJJDz9BUglJTxADgAxFFGs=";
}
