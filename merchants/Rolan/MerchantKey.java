package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Rolan
	// stagingEncryptedText: 8WEMV+fY/L83rugJiaKueQ==
	public static String msKeyValueStaging = "DGhSYzxEHTpEWW5gGlJocigYO0lDSXQLOUxoYltIPE0=";
	// gatewayEncryptedText: dkiGLfsowgIYtwOtxrp6CNpBzMx7noBi+O+6MWmR1E8=
	public static String msKeyValueGateway = "AjdFfFc0YDhUEUFzJFIke3kWAzl2SkdPeCsNFE1wNwk=";
}
