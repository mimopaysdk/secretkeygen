package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Indoapps
	// stagingEncryptedText: oS9hgEokIx+BJjdlYBJwnA==
	public static String msKeyValueStaging = "SWcSLlMaAnRoWVREIzZEOh8fRhQyU1VMYwViYH9wXRs=";
	// gatewayEncryptedText: KiBTJ2ALJSmgq2WQCZGxHQ==
	public static String msKeyValueGateway = "Z2FjGhAgLQhyP0U8NilVIQoDV2lJFEdaSFh+QnwwYEM=";
}
