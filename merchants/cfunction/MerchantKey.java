package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: cfunction
	// stagingEncryptedText: JnLuSH/Sj7WTMNiFgh9YLQ==
	public static String msKeyValueStaging = "cVoKKRxteANBdCRYZzFSIXJSTSthJnNSHmcmDG9qHnM=";
	// gatewayEncryptedText: EhOqn9X7JpoMCMtxl3CAiA==
	public static String msKeyValueGateway = "IkEVgGQDQkFbH2h3dXQaeHFCAnYccj9LU1xJd0UdPBQ=";
}
