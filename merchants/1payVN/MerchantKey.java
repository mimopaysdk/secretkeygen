package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: 1payVN
	// stagingEncryptedText: zSEsf5wcnKMIF78uWm+WeA==
	public static String msKeyValueStaging = "HwQQVWVQT0xBPCcpF1hxFWZNBywgKG1YMHVwUnE/CFw=";
	// gatewayEncryptedText: vFJao2N9NQwHwsqyObq1rw==
	public static String msKeyValueGateway = "DnxWWiQzBTN+K1s5Ln5yQy54GS4ZIzFCOn1bOXZENFE=";
}
