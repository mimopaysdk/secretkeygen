package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Westlake_Roh
	// stagingEncryptedText: XaLysKu5bB2fOT57JCUKdQ==
	public static String msKeyValueStaging = "XCEDDTJzTzwneyE2d24UWCAzU3I5Q2dgGTQmdC1PcBQ=";
	// gatewayEncryptedText: U47q8laNdUH+lTchODT7JQ==
	public static String msKeyValueGateway = "E3dDAy90aBpVdTpdSRogQB8LFT1rfFg9Xkw+X11OXF0=";
}
