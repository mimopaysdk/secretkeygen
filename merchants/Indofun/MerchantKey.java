package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Indofun
	// stagingEncryptedText: gez53CR0pagLFHq1VLcc4g==
	public static String msKeyValueStaging = "gGkPVCpPBwwgHjUcSQhHdQcZVGciH1JnNnUmQRM2VUs=";
	// gatewayEncryptedText: 1HFa+ulhyZkAAPo/K3zdOQ==
	public static String msKeyValueGateway = "WnBaJWwWQB1LOj11HWJ1cURrSxQqchBhBiURYDVTVy4=";
}
