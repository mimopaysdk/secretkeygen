package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: BoyaaBig2
	// stagingEncryptedText: DGdJmlnBdqDDiXgJ8gB8Ax861XTYvLf0LuVYgS7oLj4=
	public static String msKeyValueStaging = "CERoMHU5LXE1CxAsQVx2fVQ9HzQKWnpGNXd/UXRyTFM=";
	// gatewayEncryptedText: sxIWAKthWJ5lXylPco4S+g==
	public static String msKeyValueGateway = "f2YXd345W0k9gFNWQUZvYB9LTGI1MXZFJRchCQw9M4A=";
}
