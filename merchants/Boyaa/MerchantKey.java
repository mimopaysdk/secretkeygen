package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Boyaa
	// stagingEncryptedText: S1RGUfUjhIr6U8ubYOX9Mg==
	public static String msKeyValueStaging = "YAkXQyNGO0I4CyVLfxhsNxNyShJnP1xaNW8bMHQ/H0E=";
	// gatewayEncryptedText: fhSI5b3fjMq7UfJFhWqXYA==
	public static String msKeyValueGateway = "HXhUHiMsWFlxOQx6CAp6enkcbT5lCnEPHxEDDyFdVGU=";
}
