package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: UltraPower
	// stagingEncryptedText: 6Qq5AkohnbCOZX2/11TnHQ==
	public static String msKeyValueStaging = "RhcjCF9aCBI1QnEuezUbPENQV2pxMGhZTGwRVF0RVGY=";
	// gatewayEncryptedText: T0CvjtmMAJ9GteaXgsyNvw==
	public static String msKeyValueGateway = "QXd/JxBnCjZ7SgpVcR8hDnlcgHsEEEF/HmZ1N119Cgk=";
}
