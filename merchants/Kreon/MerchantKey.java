package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Kreon
	// stagingEncryptedText: ffQkzGf8SXOMA4qloJrPhw==
	public static String msKeyValueStaging = "HRZ9ExgQPCEeI3VtXTVcNT83SnAgFl9Jem5daBoRNVI=";
	// gatewayEncryptedText: 5CJ+CdNr8Kc5NRxysD/p4g==
	public static String msKeyValueGateway = "L3YsFzsHEisODhkNElQgJUxJfmxvJRY7WS8+NG5ZBDI=";
}
