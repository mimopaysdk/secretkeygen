package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: ChangYou
	// stagingEncryptedText: L0c7vEb+2hkahG812p0JNg==
	public static String msKeyValueStaging = "HFF0KRVrbTNBeRw5YBd+FDMUORKAYy1PGQIuGAwcE3M=";
	// gatewayEncryptedText: n3a2ZBpCbRreXrSOQQz9PJufN3pab4Cj2g/HpNzHp1o=
	public static String msKeyValueGateway = "REQqZAEFWgY3NBQEDHU4HUxEbXhOfUltdkgELy5yKFI=";
}
