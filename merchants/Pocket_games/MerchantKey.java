package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Pocket_games
	// stagingEncryptedText: JHhfheC/mjpyQbOvcbEA5g==
	public static String msKeyValueStaging = "BnwJHRBXLAVzKykme1JZLGpfahAXCTEwGwxpaU9YOXg=";
	// gatewayEncryptedText: sZqI0j+GQVTxeixh56nKWg==
	public static String msKeyValueGateway = "dWdubHpWemx8W2wJCiAoXCJTIWhfY2AkPm5dWQ8YMW8=";
}
