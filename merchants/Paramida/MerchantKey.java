package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Paramida
	// stagingEncryptedText: 2dB2/Ahp36S6/l3eFWXw/A==
	public static String msKeyValueStaging = "XHBeQBMEEgIjJwdEWBZSNQEFe2hZTl9oVFlSAmZjVlo=";
	// gatewayEncryptedText: ebwdvkc+Xff7sCYLzeN8qw==
	public static String msKeyValueGateway = "d2oJHWhvUQNNaW0RXBUoGkkuEFwJCWMLAXRYTlRaHxk=";
}
