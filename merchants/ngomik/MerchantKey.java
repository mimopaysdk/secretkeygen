package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: ngomik
	// stagingEncryptedText: LvKFoyC3M6wrV6Nux7lzeA==
	public static String msKeyValueStaging = "UBIFfDRaF1djHxw1dSlpBEI4M3pGAQpGVTAuKyYDc0A=";
	// gatewayEncryptedText: dS4sbjXWKcOXDnYg/efKJQ==
	public static String msKeyValueGateway = "bFV5TVcELRczPwdGKnkaAno2e0tif2ouPz5Wc2YlT28=";
}
