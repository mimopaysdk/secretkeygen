package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Ayala
	// stagingEncryptedText: YzJuBDtmSFdrBCa/oRvzcw==
	public static String msKeyValueStaging = "fhoUWXNCMCMBThhPECxBeV1YIiFcOyMjHh5uC1gZVyU=";
	// gatewayEncryptedText: 8awG9G80He7nKJGziaEqaQ==
	public static String msKeyValueGateway = "IT5IeAJvGw8cSUZdbxYCI3xrAU5HWx1dEBRgFlYFTAk=";
}
