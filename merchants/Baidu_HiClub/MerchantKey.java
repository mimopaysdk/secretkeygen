package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Baidu_HiClub
	// stagingEncryptedText: SEJQMsQmlbshZ/N7UvweTw==
	public static String msKeyValueStaging = "ES8PM05BCXF7eycRGBkQY0BCYG53Uy95ZWdqJC9XB0U=";
	// gatewayEncryptedText: Mtr4khEijzksVu3L+dHSEA==
	public static String msKeyValueGateway = "J2Y5QA8QQD8pZxEeFnZtHQhZHE08Ej9VaRE9RAJiHgc=";
}
