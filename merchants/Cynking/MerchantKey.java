package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Cynking
	// stagingEncryptedText: pOmMjQyOEMSNXnhOMctvWQ==
	public static String msKeyValueStaging = "dHEvbEYJY1spMBwlHyZjYzEEMXpycSsGEz5eUF5VVTs=";
	// gatewayEncryptedText: il7J3TUxMSjIENnKAxmiQA==
	public static String msKeyValueGateway = "WzQGQ1E8fSMbb3Q5ahIHbgU+cTkeNngnH3QZQiVtIm0=";
}
