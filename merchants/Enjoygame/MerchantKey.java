package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: Enjoygame
	// stagingEncryptedText: bEe5ZnZZ5AGONj7V1PRXPQ==
	public static String msKeyValueStaging = "exUOG0cvVE0HKxZlCw1JPFxCXUANA19MNUNebHV2TXg=";
	// gatewayEncryptedText: tRpPNxQMSXxOhTgewZ7pkw==
	public static String msKeyValueGateway = "JRhwShhLQhUhIhBIEmx4BwMQSSouSXhCLzBMUhguQCU=";
}
