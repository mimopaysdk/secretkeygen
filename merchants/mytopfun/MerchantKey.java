package com.mimopay.merchant;

public final class MerchantKey {
	// merchant: mytopfun
	// stagingEncryptedText: qOKe7IgkemAXoBPkdmdlZw==
	public static String msKeyValueStaging = "THpGC2sbMkM6Uw1KO0YcfmQMAQcWDhZxLUgmUwY9dmU=";
	// gatewayEncryptedText: NlApzvmNa1bAYtxzfskg/g==
	public static String msKeyValueGateway = "XmgkKUp1G0tmPRkNBRMJLiVfB1c7PBojVB9sPQkBMSI=";
}
