package com.mimopay.keygen;

import java.lang.reflect.Array;
import java.util.Random;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileWriter;
import java.io.IOException;

public class MimoKeyGen {

	private void jprintf(String s) { System.out.println(s); }

	private final String ALGO = "AES";
	private String msKeyValueStaging = null;
	private String msKeyValueGateway = null;
	private String mStagingSecretKey = null;
	private String mGatewaySecretKey = null;
	private String mMerchantName = null;
	private String mMerchantCode = null;
	private String mDestDir = null;

	public MimoKeyGen(String merchantCode, String merchantName, String stagingSecretkey, String gatewaySecretkey, String destDir)
	{
		mStagingSecretKey = stagingSecretkey;
		mGatewaySecretKey = gatewaySecretkey;
		mMerchantName = merchantName;
		mMerchantCode = merchantCode;
		mDestDir = destDir;
	}

	private void keyValueGen()
	{
		int i;
		int min = 1;
		int max = 128;
		Random rand = new Random(System.currentTimeMillis());

		byte[] b = new byte[32];
		for(i=0;i<32;i++) {
			Integer a = new Integer(rand.nextInt((max-min)+1)+min);
			b[i] = a.byteValue();
		}
		msKeyValueStaging = new String(Base64Coder.encode(b));
		//jprintf("msKeyValueStaging: " + msKeyValueStaging);

		b = new byte[32];
		for(i=0;i<32;i++) {
			Integer a = new Integer(rand.nextInt((max-min)+1)+min);
			b[i] = a.byteValue();
		}
		msKeyValueGateway = new String(Base64Coder.encode(b));
		//jprintf("msKeyValueGateway: " + msKeyValueGateway);
	}

	public void testalgor()
	{
		//String fname = "merchants/" + mMerchantName + "/MerchantKey.java";
		String fname = mDestDir + "/MerchantKey.java";
		String fnametxt = mDestDir + "/" + mMerchantName + ".txt";
		String dataEnc = "-";
		String dataDec = "-";

		try {
			FileWriter writer = new FileWriter(fname);
			FileWriter writertxt = new FileWriter(fnametxt);
			writer.append("package com.mimopay.merchant;\n\n");
			writer.append("public final class MerchantKey {\n");
			writer.append("\t// merchant: " + mMerchantName + "\n");
			writertxt.append("Merchant Code: " + mMerchantCode + "\n");
			writertxt.append("Merchant Name: " + mMerchantName + "\n");

			dataEnc = aesEncrypt(msKeyValueStaging, mStagingSecretKey);
			dataDec = aesDecrypt(msKeyValueStaging, dataEnc);
			writer.append("\t// stagingEncryptedText: " + dataEnc + "\n");
			writertxt.append("Staging Secret Key: " + mStagingSecretKey + "\n");
			writertxt.append("Staging Encrypted Text: " + dataEnc + "\n");
			writer.append("\tpublic static String msKeyValueStaging = \"" + msKeyValueStaging + "\";\n");
			jprintf("stagingDecryptedText: " + dataDec);

			dataEnc = aesEncrypt(msKeyValueGateway, mGatewaySecretKey);
			dataDec = aesDecrypt(msKeyValueGateway, dataEnc);
			writer.append("\t// gatewayEncryptedText: " + dataEnc + "\n");
			writertxt.append("Gateway Secret Key: " + mGatewaySecretKey + "\n");
			writertxt.append("Gateway Encrypted Text: " + dataEnc + "\n");
			writer.append("\tpublic static String msKeyValueGateway = \"" + msKeyValueGateway + "\";\n");
			jprintf("gatewayDecryptedText: " + dataDec);

			writer.append("}\n");
			writer.flush();
			writertxt.flush();
			writer.close();
			writertxt.close();
			jprintf("File " + fname + " and " + fnametxt + " generated.");

		} catch(Exception e) { jprintf("e = " + e.toString()); }
	}

	private String aesEncrypt(String sbase64keyval, String Data) throws Exception
	{
		Key key = generateKey(sbase64keyval);
		Cipher c = Cipher.getInstance(ALGO);
		c.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = c.doFinal(Data.getBytes());
		//String encryptedValue = new Base64Encoder().encode(encVal);
		String encryptedValue = new String(Base64Coder.encode(encVal));
		return encryptedValue;
	}

	private String aesDecrypt(String sbase64keyval, String encryptedData) throws Exception
	{
		Key key = generateKey(sbase64keyval);
		Cipher c = Cipher.getInstance(ALGO);
		c.init(Cipher.DECRYPT_MODE, key);
		//byte[] decordedValue = new Base64Decoder().decodeBuffer(encryptedData);
		byte[] decordedValue = Base64Coder.decode(encryptedData);
		byte[] decValue = c.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;
	}

	private Key generateKey(String sbase64keyval) throws Exception
	{
		byte[] b = Base64Coder.decode(sbase64keyval);
		Key key = new SecretKeySpec(b, ALGO);
		return key;
	}

    public static void main(String argv[])
	{
		int argc = 0;
		while(argc < Array.getLength(argv)) {
			argc++;
		}

		if(argc != 5) {
			//System.out.println("example: java -jar <merchantName> <stagingSecretKey> <gatewaySecretKey> <destdir>");
			System.out.println("example: java -jar <merchantCode> <merchantName> <stagingSecretKey> <gatewaySecretKey> <destdir>");
			System.exit(0);
		}

		System.out.println("param1: " + argv[0] + " param2: " + argv[1] + " param3: " + argv[2] + " param4: " + argv[3] + " param5: " + argv[4]);

		MimoKeyGen mkg = new MimoKeyGen(argv[0], argv[1], argv[2], argv[3], argv[4]);
		mkg.keyValueGen();
		mkg.testalgor();
    }
}
