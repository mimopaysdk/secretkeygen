package com.mimopay.merchant;

import java.util.Random;
import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public final class Merchant
{
	private static final String ALGO = "AES";
	
	public static String get(boolean staging, String encryptedSecretKey) throws Exception
	{
		byte[] b = MerchantBase64.decode(staging ? MerchantKey.msKeyValueStaging : MerchantKey.msKeyValueGateway);
		Key key = new SecretKeySpec(b, ALGO);
		Cipher c = Cipher.getInstance(ALGO);
		c.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = MerchantBase64.decode(encryptedSecretKey);
		byte[] decValue = c.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;
	}
}

